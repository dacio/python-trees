# Python Trees

Various tree objects that I've implemented or I'm currently implementing in pure Python.

# Trie

This is a simple [trie](https://en.wikipedia.org/wiki/Trie) class.

## Functionality

### `__setitem__(key, value)`

Recursively sets the value for a given key within the trie's subtries.

### `__getitem__(key)`

NOT IMPLEMENTED

### `__delitem__(key)`

NOT IMPLEMENTED

### `find_closest(key)`

Returns the value of the longest matching prefix

# Radix Tree

This is a simple [radix tree](https://en.wikipedia.org/wiki/Radix_tree) based on the [trie](#trie)

## Functionality

Same as [trie](#trie)
