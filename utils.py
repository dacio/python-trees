from typing import Tuple, Any

State = Tuple[Any]

class PickleMixin:
  def __getstate__(self) -> State:
    return tuple(getattr(self, attr) for attr in self.__slots__)

  def __setstate__(self, state: State) -> None:
    for attr, value in zip(self.__slots__, state):
      setattr(self, attr, value)
